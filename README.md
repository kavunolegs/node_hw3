**Required for app:**

- Node.js to a version higher than or equal to 15.10.0

**How to start**

- [ ] After downloading repo use 'npm install' for downloading needed module
- [ ] Use 'npm start' for starting server

